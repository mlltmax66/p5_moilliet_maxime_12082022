# Projet 5 OpenClassrooms - Blog PHP 📚

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ecbc02caaf524909b61d7f468c887950)](https://www.codacy.com/gl/mlltmax66/p5_moilliet_maxime_12082022/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mlltmax66/p5_moilliet_maxime_12082022&amp;utm_campaign=Badge_Grade)

Create blog POO PHP with design pattern MVC.

## Prérequis

PHP >= 8.0 and Composer must be installed or Docker.

## Dependencies

- 🚀 [Altorouter](http://altorouter.com),
- 🌱 [Twig](https://twig.symfony.com/doc/3.x/),
- ✉ [symfony/http-foundation](https://symfony.com/doc/current/components/http_foundation.html).
- 💥 [symfony/var-dumper](https://symfony.com/doc/current/components/var_dumper.html),
- 🛢 [robmorgan/phinx](https://phinx.org),
- 🗃️ [vlucas/valitron](https://github.com/vlucas/valitron),
- 🗂️ [fakerphp/faker](https://fakerphp.github.io),
- 🔒 [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv),
- 📚 [CKEditor 5](https://ckeditor.com).
- 📧 [PHPMailer](https://github.com/PHPMailer/PHPMailer).

## Install

Clone this repository :

```bash
git clone https://gitlab.com/mlltmax66/p5_moilliet_maxime_12082022
```

```bash
cd ./p5_moilliet_maxime_12082022
```

### With Docker :

**Step 1 :** Add infos in .env (example in .env.example).

**Step 2:** Install project :

```bash
docker compose up
```

**Step 3:** go into container :

```bash
docker exec -it php-apache bash
```

**Step 4:** Install dependencies in container :

```bash
composer install
```

**Step 5 :** Run migration in container :
```bash
php vendor/bin/phinx migrate
```

**Step 6 :** Run seeder in container :
```bash
php vendor/bin/phinx seed:run
```

Project run on port 8000 and phpmyadmin on port 8080.

**phpmyadmin infos** :

    - user : MYSQL_USER
    - password : MYSQL_PASSWORD

### With php and composer :

**Step 1:** Install dependencies :

```bash
composer install
```

**Step 2 :** Create database.

**Step 3 :** Add your db infos .env.

**Step 4 :** Run migration
```bash
php vendor/bin/phinx migrate
```

**Step 5 :** Run seeder
```bash
php vendor/bin/phinx seed:run
```

**Step 6 :** Start project :

```bash
php -S localhost:8000 -t public
```

