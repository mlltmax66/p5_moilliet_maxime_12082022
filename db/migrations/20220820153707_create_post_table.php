<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreatePostTable extends AbstractMigration
{
    public function change(): void
    {
        $this->table('post')
            ->addColumn('title', 'string')
            ->addColumn('teaser', 'text')
            ->addColumn('content', 'text')
            ->addColumn('user_id', 'integer', ['null' => true])
            ->addForeignKey('user_id', 'user', 'id', ['delete' => 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->addColumn('updated_at', 'datetime')
            ->addColumn('created_at', 'datetime')
            ->create();
    }
}
