<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateCommentTable extends AbstractMigration
{
    public function change(): void
    {
        $this->table('comment')
            ->addColumn('content', 'text')
            ->addColumn('is_published', 'boolean')
            ->addColumn('user_id', 'integer', ['null' => true])
            ->addForeignKey('user_id', 'user', 'id', ['delete' => 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->addColumn('post_id', 'integer', ['null' => true])
            ->addForeignKey('post_id', 'post', 'id', ['delete' => 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->addColumn('created_at', 'datetime')
            ->create();
    }
}
