<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class CommentSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'UserSeeder',
            'PostSeeder'
        ];
    }

    public function run()
    {
        $faker = Factory::create('fr_FR');
        $data = [];
        for ($i = 0; $i < 80; $i++) {
            $data[] = [
                'content' => $faker->paragraph(rand(4, 6)),
                'is_published' => rand(0, 1),
                'user_id' => rand(2, 50),
                'post_id' => rand(1, 20),
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $posts = $this->table('comment');
        $posts->insert($data)
            ->saveData();
    }
}
