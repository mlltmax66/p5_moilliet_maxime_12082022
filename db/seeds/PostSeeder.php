<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class PostSeeder extends AbstractSeed
{
    public function getDependencies(): array
    {
        return [
            'UserSeeder'
        ];
    }

    public function run()
    {
        $faker = Factory::create('fr_FR');
        $data = [];
        for ($i = 0; $i < 20; $i++) {
            $data[] = [
                'title' => $faker->sentence(),
                'teaser' => $faker->paragraph(rand(7, 12)),
                'content' => "<h2>The three greatest things you learn from traveling </h2 ><p>&nbsp;</p>
                    <p>Like all the great things on earth traveling teaches us by example. 
                    Here are some of the most precious lessons I’ve learned over the years of traveling .</p>
                    <p >&nbsp;</p><h3> Appreciation of diversity</h3><p>&nbsp;</p><p>
                    Getting used to an entirely different culture can be challenging. while it’s also nice to learn 
                    about cultures online or from books, nothing comes close to experiencing cultural diversity in 
                    person . You learn to appreciate each and every single one of the differences while you become 
                    more culturally fluid .</p><p>&nbsp;</p><blockquote><p>The real voyage of discovery consists 
                    not in seeking new landscapes, but having new eyes.</p><p><strong>Marcel Proust </strong></p>
                    </blockquote><h3 >&nbsp;</h3><h3> Improvisation</h3><p>&nbsp;</p><p> Life doesn't allow us to
                    execute every single plan perfectly. This especially seems to be the case when you travel. You 
                    plan it down to every minute with a big checklist; but when it comes to executing it, something 
                    always comes up and you’re left with your improvising skills. You learn to adapt as you go. Here’s
                    how my travel checklist looks now:</p><p>&nbsp;</p><h3>list of courses :</h3><p>&nbsp;</p>
                    <ol><li> badminton,</li ><li> tenis,</li><li> ping - pong</li><li> etc…</li></ol><p>&nbsp;</p><h3> 
                    list of courses :</h3><p>&nbsp;</p><ul><li>badminton,</li ><li>tenis,</li><li>ping - pong</li>
                    <li>etc…</li></ul><p>&nbsp;</p><h3> Confidence</h3><p>&nbsp;</p><p>Going to a new place can be quite
                    terrifying. while change and uncertainty make us scared, traveling teaches us how ridiculous
                    it is to be afraid of something before it happens . The moment you face your fear and see there was
                    nothing to be afraid of, is the moment you discover bliss .</p>",
                'user_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        $posts = $this->table('post');
        $posts->insert($data)
            ->saveData();
    }
}
