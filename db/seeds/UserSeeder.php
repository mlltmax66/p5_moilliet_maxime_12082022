<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    public function run()
    {
        $faker = Factory::create('fr_FR');
        $data = [];
        for ($i = 0; $i < 50; $i++) {
            $data[] = [
                'name' => $faker->name(),
                'email' => $faker->email(),
                'password' => '$2y$10$PtEpKQ/huTS3h0eX9QtkwOAY8WzG0D7R5fx0QVgZsNCtZzD0rwNqS', // password => password
                'is_admin' => $i === 0 ? 1 : 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        $posts = $this->table('user');
        $posts->insert($data)
            ->saveData();
    }
}
