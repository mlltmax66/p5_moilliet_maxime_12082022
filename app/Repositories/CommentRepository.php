<?php

namespace App\Repositories;

use JetBrains\PhpStorm\ArrayShape;

/**
 * Example $filters = ['id' => 3];
 * Example $orderBy = ['createdAt' => 'DESC'];
 * @method fetchAll(array $filters = [], array $orderBy = [], ?int $length = null, ?int $start = null): array
 * @method fetchAllQuery(string $sqlQuery, array $params = []): array
 * @method findOneBy(array $filters = [])
 * @method find(int $id)
 * @method paginate(int $page, int $perPage, array $filters = []): array
 * $param = name of database column for where like query
 * $paramValue = value for where like query
 * @method getByOrPaginate(int $page, int $perPage, string $param, ?string $paramValue, array $orderBy = []): array
 */
class CommentRepository extends AbstractRepository
{
    public function getCommentsOfPost(int $postId): array
    {
        $sqlQuery = /** @lang text */
            "SELECT c.content, c.created_at, u.name AS username FROM comment c 
            LEFT JOIN user u ON c.user_id = u.id 
            Left JOIN post p ON c.post_id = p.id
            WHERE c.is_published = '1' AND p.id = :postId
            ORDER BY c.created_at DESC";

        return $this->fetchAllQuery($sqlQuery, ['postId' => $postId]);
    }

    #[ArrayShape(['data' => "array|false", 'page' => "int", 'total_pages' => "int", 'nextPage' => "bool", 'prevPage' => "bool"])]
    public function getCommentsFilterOrPaginate(int $page, int $perPage, string $title, int $isPublished): array
    {
        $paramsQuery = [];
        $selectQuery = /** @lang text */
            "SELECT c.id, c.content, c.is_published, c.created_at, u.name AS username, 
        u.id AS user_id, p.id AS post_id, p.title AS post_title FROM comment c ";
        $joinQuery = /** @lang text */
            "LEFT JOIN post p ON c.post_id = p.id LEFT JOIN user u ON c.user_id = u.id";
        $sqlQueryCount = /** @lang text */
            "SELECT COUNT(*) as count_data FROM comment c " . $joinQuery;

        $sqlQuery = sprintf($selectQuery . $joinQuery . /** @lang text */ "
            ORDER BY c.created_at DESC
            %s
        ", $this->limit($perPage, $this->getPaginateStart($page, $perPage)));

        $isPublishedFormatted = $isPublished == 2 ? 1 : 0;

        if ($title != '' && $isPublished > 1) {
            $sqlQueryCount = sprintf($sqlQueryCount . /** @lang text */
                "WHERE p.title Like ? AND is_published = '%s'", $isPublishedFormatted);
            $sqlQuery = sprintf($selectQuery . $joinQuery . /** @lang text */ "
                %s AND is_published = '%s'
                ORDER BY c.created_at DESC", /** @lang text */ "WHERE p.title Like ? ", $isPublishedFormatted);
            $paramsQuery = array('%' . $title . '%');
        } else if ($title == '' && $isPublished > 1) {
            $sqlQueryCount = $sqlQueryCount . /** @lang text */
                "WHERE is_published = '" . $isPublishedFormatted . "'";
            $sqlQuery = sprintf($selectQuery . $joinQuery . /** @lang text */ "
                %s
                ORDER BY c.created_at DESC
                ", /** @lang text */ "WHERE is_published = '" . $isPublishedFormatted . "'");
        } else if ($title != '' && $isPublished <= 1) {
            $sqlQuery = sprintf(/** @lang text */ "
                SELECT c.id, c.content, c.is_published,c.created_at, u.name AS username, u.id AS user_id, p.id AS post_id, p.title AS post_title FROM comment c
                LEFT JOIN post p ON c.post_id = p.id
                LEFT JOIN user u ON c.user_id = u.id
                %s
                ORDER BY c.created_at DESC", /** @lang text */ "WHERE p.title Like ? ");
            $paramsQuery = array('%' . $title . '%');
        }


        if ($title == '' && $isPublished <= 1) {
            $total_pages = $this->getTotalPages($sqlQueryCount, $perPage);
            ($page < 0 || $total_pages < $page) && $page = 0;

            $comments = $this->fetchAllQuery($sqlQuery);
            return [
                'data' => $comments,
                'page' => $page === 0 ? 1 : $page,
                'total_pages' => $total_pages,
                'nextPage' => $this->haveNextPage($page, $total_pages),
                'prevPage' => $this->havePrevPage($page),
            ];

        }
        $comments = $this->fetchAllQuery($sqlQuery, $paramsQuery);
        return [
            'data' => $comments,
        ];
    }
}