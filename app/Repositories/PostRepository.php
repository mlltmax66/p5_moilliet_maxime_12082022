<?php

namespace App\Repositories;

/**
 * Example $filters = ['id' => 3];
 * Example $orderBy = ['createdAt' => 'DESC'];
 * @method fetchAll(array $filters = [], array $orderBy = [], ?int $length = null, ?int $start = null): array
 * @method fetchAllQuery(string $sqlQuery, array $params = []): array
 * @method findOneBy(array $filters = [])
 * @method find(int $id)
 * @method paginate(int $page, int $perPage, array $filters = []): array
 * $param = name of database column for where like query
 * $paramValue = value for where like query
 * @method getByOrPaginate(int $page, int $perPage, string $param, ?string $paramValue, array $orderBy = []): array
 */
class PostRepository extends AbstractRepository
{
    public function postsLatest(): array
    {
        return $this->fetchAll([], ['createdAt' => 'desc'], 3);
    }
}