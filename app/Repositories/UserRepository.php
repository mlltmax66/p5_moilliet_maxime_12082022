<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Example $filters = ['id' => 3];
 * Example $orderBy = ['createdAt' => 'DESC'];
 * @method fetchAll(array $filters = [], array $orderBy = [], ?int $length = null, ?int $start = null): array
 * @method fetchAllQuery(string $sqlQuery, array $params = []): array
 * @method findOneBy(array $filters = [])
 * @method find(int $id)
 * @method paginate(int $page, int $perPage, array $filters = []): array
 * $param = name of database column for where like query
 * $paramValue = value for where like query
 * @method getByOrPaginate(int $page, int $perPage, string $param, ?string $paramValue, array $orderBy = []): array
 */
class UserRepository extends AbstractRepository
{
    public function getComments(User $user): array
    {
        $sqlQuery = /** @lang text */
            "SELECT c.id, c.content, c.created_at, p.title AS post_title, p.id AS post_id FROM comment c
            LEFT JOIN user u ON c.user_id = u.id
            LEFT JOIN post p ON c.post_id = p.id
            WHERE u.id = :userId
            ORDER BY c.created_at DESC";

        return $this->fetchAllQuery($sqlQuery, ['userId' => $user->getId()]);
    }

    public function getPosts(User $user): array
    {
        $sqlQuery = /** @lang text */
            "SELECT p.id, p.title, p.teaser, p.created_at FROM post p
            LEFT JOIN user u ON p.user_id = u.id
            WHERE u.id = :userId
            ORDER BY p.created_at DESC";

        return $this->fetchAllQuery($sqlQuery, ['userId' => $user->getId()]);
    }
}