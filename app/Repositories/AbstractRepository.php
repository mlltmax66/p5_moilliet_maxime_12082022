<?php

namespace App\Repositories;

use App\Database\ORMException;
use App\Models\AbstractModel;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionClass;
use ReflectionException;

abstract class AbstractRepository
{
    protected \PDO $pdo;

    protected string $model;

    protected array $metadata;

    /**
     * @throws ORMException
     * @throws ReflectionException
     */
    public function __construct(\PDO $pdo, string $model)
    {
        $this->pdo = $pdo;
        $reflectionClass = new ReflectionClass($model);
        if ($reflectionClass->getParentClass()->getName() == AbstractModel::class) {
            $this->model = $model;
            $this->metadata = $this->model::metadata();
        } else {
            throw new ORMException("This class is not Entity.");
        }
        $this->model = $model;
    }

    private function getColumnByProperty(string $property): int|string|null
    {
        $property = lcfirst($property);
        $columns = array_keys(array_filter($this->metadata["columns"], function ($column) use ($property) {
            return $column["property"] == $property;
        }));
        return array_shift($columns);
    }

    public function where(array $filters = []): string
    {
        if (!empty($filters)) {
            $conditions = [];
            foreach ($filters as $property => $value) {
                $conditions[] = sprintf("%s = :%s", $this->getColumnByProperty($property), $property);
            }
            return sprintf("WHERE %s", implode(" AND ", $conditions));
        }
        return "";
    }

    public function orderBy(array $sorting = []): string
    {
        if (!empty($sorting)) {
            $sorts = [];
            foreach ($sorting as $property => $value) {
                $sorts[] = sprintf("%s %s", $this->getColumnByProperty($property), $value);
            }
            return sprintf("ORDER BY %s", implode("", $sorts));
        }
        return "";
    }

    public function limit(?int $length, ?int $start): string
    {
        if ($length !== null) {
            if ($start !== null) {
                return sprintf("LIMIT %s,%s", $start, $length);
            }
            return sprintf("LIMIT %s", $length);
        }
        return "";
    }

    public function fetch(array $filters = [])
    {
        $sqlQuery = sprintf(/** @lang text */ "SELECT * FROM %s %s LIMIT 0,1", $this->metadata["table"],
            $this->where($filters));
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute($filters);
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        if ($result) {
            return (new $this->model())->hydrate($result);
        }
        return $result;
    }

    public function fetchAll(array $filters = [], array $orderBy = [], ?int $length = null, ?int $start = null): array
    {
        $sqlQuery = sprintf(/** @lang text */ "SELECT * FROM %s %s %s %s", $this->metadata["table"],
            $this->where($filters), $this->orderBy($orderBy), $this->limit($length, $start));
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute($filters);
        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $data = [];
        foreach ($results as $result) {
            $data[] = (new $this->model())->hydrate($result);
        }
        return $data;
    }

    public function fetchAllQuery(string $sqlQuery, array $params = []): array
    {
        $stmt = $this->pdo->prepare($sqlQuery);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findOneBy(array $filters = [])
    {
        return $this->fetch($filters);
    }

    public function find(int $id)
    {
        return $this->fetch([$this->metadata["primaryKey"] => $id]);
    }

    public function getByOrPaginate(int $page, int $perPage, string $param, ?string $paramValue, array $orderBy = []): array
    {
        if (!$paramValue) {
            return $this->paginate($page, $perPage);
        }
        $sqlQuery = sprintf(/** @lang text */ "SELECT * FROM %s WHERE %s LIKE ? %s", $this->metadata["table"],
            $param, $this->orderBy($orderBy));
        $stmt = $this->pdo->prepare($sqlQuery);
        $stmt->execute(array("%$paramValue%"));
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $data = [];
        foreach ($results as $result) {
            $data[] = (new $this->model())->hydrate($result);
        }
        return [
            'data' => $data
        ];
    }

    #[ArrayShape(['data' => "array", 'page' => "int|mixed", 'total_pages' => "int", 'nextPage' => "bool", 'prevPage' => "bool"])]
    public function paginate(int $page, int $perPage, array $filters = []): array
    {
        if (!empty($filters)) {
            $sqlQueryCount = sprintf(/** @lang text */ "SELECT COUNT(*) as count_data FROM %s %s", $this->metadata['table'], $this->where($filters));
        } else {
            $sqlQueryCount = /** @lang text */
                "SELECT COUNT(*) as count_data FROM " . $this->metadata['table'];
        }
        $total_pages = $this->getTotalPages($sqlQueryCount, $perPage, $filters);
        ($page < 0 || $total_pages < $page) && $page = 0;
        return [
            'data' => $this->fetchAll($filters, ['createdAt' => 'DESC'], $perPage, $this->getPaginateStart($page, $perPage)),
            'page' => $page === 0 ? 1 : $page,
            'total_pages' => $total_pages,
            'nextPage' => $this->haveNextPage($page, $total_pages),
            'prevPage' => $this->havePrevPage($page),
        ];
    }

    public function getTotalPages(string $query, int $perPage, array $filters = []): int
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($filters);
        $count = $statement->fetchAll();
        return (integer)number_format(ceil($count[0]['count_data'] / $perPage), 0);
    }

    public function haveNextPage(int $page, int $total_pages): bool
    {
        return $page < $total_pages;
    }

    public function havePrevPage(int $page): bool
    {
        return $page > 1;
    }

    public function getPaginateStart(int $page, int $perPage): int
    {
        return $page === 0 ? $page : ($page - 1) * $perPage;
    }
}