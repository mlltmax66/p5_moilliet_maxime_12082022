<?php

namespace App\Forms;

use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\EmailType;
use App\Models\User;

class UpdateUserEmailForm extends AbstractForm
{
    public function form(User $user): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('update_user_email')
            ->setFormLabel('Modifier l\'email d\'un utilisateur')
            ->setSubmitButtonLabel('Modifier')
            ->setAction('/admin/users/' . $user->getId() . '/store-email')
            ->addEntry(new EmailType(), 'email', 'Adresse mail', true, $user->getEmail())
            ->get();
    }
}