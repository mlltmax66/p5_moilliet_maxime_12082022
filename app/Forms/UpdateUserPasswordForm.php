<?php

namespace App\Forms;

use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\PasswordType;
use App\Models\User;

class UpdateUserPasswordForm extends AbstractForm
{
    public function form(User $user): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('update_user_password')
            ->setFormLabel('Modifier le mot de passe d\'un utilisateur')
            ->setSubmitButtonLabel('Modifier')
            ->setAction('/admin/users/' . $user->getId() . '/store-password')
            ->addEntry(new PasswordType(), 'password', 'Nouveau Mot de passe')
            ->addEntry(new PasswordType(), 'confirm_password', 'Confirmer le mot de passe')
            ->get();
    }
}