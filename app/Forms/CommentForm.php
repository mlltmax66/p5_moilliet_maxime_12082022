<?php

namespace App\Forms;

use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\TextLongType;
use App\Forms\FormBuilder\Types\TextType;

class CommentForm extends AbstractForm
{
    public function form(int $postId): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('create_comment')
            ->setSubmitButtonLabel('Envoyer')
            ->setAction('/posts/' . $postId . '/comment')
            ->addEntry(new TextLongType(), 'content', 'Votre message')
            ->get();
    }
}