<?php

namespace App\Forms;

use Symfony\Component\HttpFoundation\Session\Session;

abstract class AbstractForm
{
    public function __construct(protected Session $session)
    {
    }
}