<?php

namespace App\Forms;

use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\EditorType;
use App\Forms\FormBuilder\Types\TextLongType;
use App\Forms\FormBuilder\Types\TextType;
use App\Models\Post;

class PostForm extends AbstractForm
{
    public function form(?Post $post = null): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName($post ? 'update_post' : 'create_post')
            ->setFormLabel($post ? 'Modifier un article' : 'Ajouter un article')
            ->setSubmitButtonLabel($post ? 'Modifier' : 'Ajouter')
            ->setAction($post ? '/admin/posts/' . $post->getId() . '/update' : '/admin/posts/create')
            ->addEntry(new TextType(), 'title', 'Titre', true,
                $post ? $post->getTitle() : '')
            ->addEntry(new TextLongType(), 'teaser', 'Introduction', true,
                $post ? $post->getTeaser() : '')
            ->addEntry(new EditorType(), 'content', 'Contenu', true,
                $post ? $post->getContent() : '')
            ->get();
    }
}