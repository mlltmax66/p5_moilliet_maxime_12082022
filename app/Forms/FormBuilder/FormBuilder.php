<?php

namespace App\Forms\FormBuilder;

use App\Forms\FormBuilder\Types\TypeInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class FormBuilder
{
    private string $uniqueName;

    private array $form;

    public function __construct(private Session $session)
    {
    }

    public function setFormLabel(string $name): static
    {
        $this->form['name'] = $name;
        return $this;
    }

    public function setSubmitButtonLabel(string $name): static
    {
        $this->form['button_name'] = $name;
        return $this;
    }

    public function setAction(string $action): static
    {
        $this->form['action'] = $action;
        return $this;
    }

    public function setUniqueName(string $uniqueName): static
    {
        $this->uniqueName = $uniqueName;
        return $this;
    }

    public function addEntry(
        TypeInterface $type,
        string        $name,
        string        $label,
        bool          $required = true,
        string        $defaultValue = '',
        string        $placeholder = ''
    ): static
    {
        $infos = [
            'type' => $type->getName(),
            'name' => $name,
            'label' => $label,
            'required' => $required,
            'placeholder' => $placeholder,
            'defaultValue' => $defaultValue
        ];
        $this->form['entries'][] = $infos;
        return $this;
    }

    public function get(): array
    {
        $this->makeCsrfToken();
        return $this->form;
    }

    private function makeCsrfToken(): void
    {
        $token = bin2hex(random_bytes(32));
        $this->form['unique_name'] = $this->uniqueName;
        $this->form['csrf_token'] = $token;
        $this->session->set('csrf_token_' . $this->uniqueName, $token);
    }
}