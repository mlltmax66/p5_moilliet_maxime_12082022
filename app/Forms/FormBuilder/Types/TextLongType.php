<?php

namespace App\Forms\FormBuilder\Types;

class TextLongType implements TypeInterface
{
    public function getName(): string
    {
        return 'textarea';
    }
}