<?php

namespace App\Forms\FormBuilder\Types;

class EmailType implements TypeInterface
{
    public function getName(): string
    {
        return 'email';
    }
}