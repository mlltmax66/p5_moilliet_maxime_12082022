<?php

namespace App\Forms\FormBuilder\Types;

class PasswordType implements TypeInterface
{
    public function getName(): string
    {
        return 'password';
    }
}