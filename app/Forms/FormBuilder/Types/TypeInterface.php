<?php

namespace App\Forms\FormBuilder\Types;

interface TypeInterface
{
    public function getName(): string;
}