<?php

namespace App\Forms\FormBuilder\Types;

class TextType implements TypeInterface
{
    public function getName(): string
    {
        return 'text';
    }
}