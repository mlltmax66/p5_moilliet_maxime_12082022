<?php

namespace App\Forms\FormBuilder\Types;

class EditorType implements TypeInterface
{
    public function getName(): string
    {
        return 'editor';
    }
}