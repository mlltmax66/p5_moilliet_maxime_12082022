<?php

namespace App\Forms;

use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\EmailType;
use App\Forms\FormBuilder\Types\TextLongType;
use App\Forms\FormBuilder\Types\TextType;

class ContactForm extends AbstractForm
{
    public function form(): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('contact')
            ->setSubmitButtonLabel('Envoyer')
            ->setAction('/contact')
            ->addEntry(new EmailType(), 'email', 'Email')
            ->addEntry(new TextType(), 'subject', 'Sujet')
            ->addEntry(new TextLongType(), 'message', 'Message')
            ->get();
    }
}