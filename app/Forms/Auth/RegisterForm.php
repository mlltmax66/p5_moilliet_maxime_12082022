<?php

namespace App\Forms\Auth;

use App\Forms\AbstractForm;
use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\EmailType;
use App\Forms\FormBuilder\Types\PasswordType;
use App\Forms\FormBuilder\Types\TextType;

class RegisterForm extends AbstractForm
{
    public function form(): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('register_user')
            ->setSubmitButtonLabel('S\'inscrire')
            ->setAction('/register')
            ->addEntry(new TextType(), 'name', 'Votre prénom et nom')
            ->addEntry(new EmailType(), 'email', 'Votre adresse mail')
            ->addEntry(new PasswordType(), 'password', 'Mot de passe')
            ->addEntry(new PasswordType(), 'confirm_password', 'Confirmer votre mot de passe')
            ->get();
    }
}