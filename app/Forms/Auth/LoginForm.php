<?php

namespace App\Forms\Auth;

use App\Forms\AbstractForm;
use App\Forms\FormBuilder\FormBuilder;
use App\Forms\FormBuilder\Types\EmailType;
use App\Forms\FormBuilder\Types\PasswordType;

class LoginForm extends AbstractForm
{
    public function form(): array
    {
        return (new FormBuilder($this->session))
            ->setUniqueName('login_user')
            ->setSubmitButtonLabel('Se connecter')
            ->setAction('/login')
            ->addEntry(new EmailType(), 'email', 'Adresse mail')
            ->addEntry(new PasswordType(), 'password', 'Mot de passe')
            ->get();
    }
}