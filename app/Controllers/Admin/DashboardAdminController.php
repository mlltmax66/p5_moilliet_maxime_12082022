<?php

namespace App\Controllers\Admin;

use App\Controllers\AbstractAdminController;
use Symfony\Component\HttpFoundation\Response;

class DashboardAdminController extends AbstractAdminController
{
    public function index(): Response
    {
        return $this->render('admin/dashboard', [
            'user' => $this->getSession()->get('auth')
        ]);
    }
}