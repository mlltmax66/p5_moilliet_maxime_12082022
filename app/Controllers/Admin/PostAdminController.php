<?php

namespace App\Controllers\Admin;

use App\Controllers\AbstractAdminController;
use App\Forms\PostForm;
use App\Models\Post;
use App\Requests\StorePostRequest;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostAdminController extends AbstractAdminController
{
    public function index(): Response
    {
        $request = Request::createFromGlobals();
        $page = (integer)($request->query->get('page') ?? 0);
        $search = $request->query->get('search');

        return $this->render('admin/posts/index', [
            'postsPaginate' => $this->getRepository(Post::class)->getByOrPaginate($page, 10, 'title', $search, [
                'createdAt' => 'DESC'
            ])
        ]);
    }

    public function create(): Response
    {
        return $this->render('admin/posts/create', [
            'form' => (new PostForm($this->getSession()))->form()
        ]);
    }

    public function store(): Response
    {
        $storePostRequest = (new StorePostRequest($this->getSession()))->rules();
        if ($storePostRequest->validate()) {
            $post = new Post();
            $post->setTitle($storePostRequest->request->get('title'));
            $post->setTeaser($storePostRequest->request->get('teaser'));
            $post->setContent($storePostRequest->request->get('content'));
            $post->setUserId($storePostRequest->session->get('auth')->getId());
            $post->setCreatedAt(new DateTime());
            $post->setUpdatedAt(new DateTime());

            $this->manager->persist($post);

            $this->getSession()->getFlashBag()->add('success', 'L\'article à bien été créé avec succès.');

            return $this->redirectTo('/admin/posts');
        }
        return $this->redirectTo('/admin/posts/create');
    }

    public function edit(int $postId): Response
    {
        $post = $this->getRepository(Post::class)->find($postId);
        if ($post) {
            return $this->render('admin/posts/edit', [
                'form' => (new PostForm($this->getSession()))->form($post)
            ]);
        }
        return $this->redirectTo('/404');
    }

    public function update(int $postId): Response
    {
        $storePostRequest = (new StorePostRequest($this->getSession()))->rules();
        $post = $this->getRepository(Post::class)->find($postId);
        if (!$post) {
            return $this->redirectTo('/404');
        }

        if ($storePostRequest->validate()) {
            $post->setTitle($storePostRequest->request->get('title'));
            $post->setTeaser($storePostRequest->request->get('teaser'));
            $post->setContent($storePostRequest->request->get('content'));
            $post->setUpdatedAt(new DateTime());

            $this->manager->persist($post);

            $this->getSession()->getFlashBag()->add('success', 'L\'article à bien été modifié avec succès.');

            return $this->redirectTo('/admin/posts');
        }

        return $this->redirectTo('/admin/posts/' . $postId . '/edit');
    }

    public function destroy(int $postId): Response
    {
        $post = $this->getRepository(Post::class)->find($postId);
        if ($post) {
            $this->manager->remove($post);

            $this->getSession()->getFlashBag()->add('success', 'L\'article à bien été supprimé avec succès.');

            return $this->redirectTo('/admin/posts');
        }

        return $this->redirectTo('/admin/posts');
    }
}