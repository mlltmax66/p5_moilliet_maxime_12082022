<?php

namespace App\Controllers\Admin;

use App\Controllers\AbstractAdminController;
use App\Models\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentAdminController extends AbstractAdminController
{
    public function index(): Response
    {
        $request = Request::createFromGlobals();
        $page = (integer)($request->query->get('page') ?? 0);
        $title = $request->query->get('search') ?? '';
        $isPublished = (integer)$request->query->get('is_published') ?? 0;

        return $this->render('admin/comments/index', [
            'commentsPaginate' => $this->getRepository(Comment::class)
                ->getCommentsFilterOrPaginate($page, 10, $title, $isPublished)
        ]);
    }

    public function togglePublished(int $commentId): Response
    {
        $comment = $this->getRepository(Comment::class)->find($commentId);
        if ($comment) {
            $comment->setIsPublished(!$comment->getIsPublished());

            $this->manager->persist($comment);

            $this->getSession()->getFlashBag()
                ->add('success', 'Le commentaire à bien changé de statut avec succès.');

            return $this->redirectTo('/admin/comments');
        }

        return $this->redirectTo('404');
    }

    public function destroy(int $commentId): Response
    {
        $comment = $this->getRepository(Comment::class)->find($commentId);
        if ($comment) {
            $this->manager->remove($comment);

            $this->getSession()->getFlashBag()
                ->add('success', 'Le commentaire à bien été supprimé avec succès.');

            return $this->redirectTo('/admin/comments');
        }

        return $this->redirectTo('/404');
    }
}