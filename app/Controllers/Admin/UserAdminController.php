<?php

namespace App\Controllers\Admin;

use App\Controllers\AbstractAdminController;
use App\Forms\UpdateUserEmailForm;
use App\Forms\UpdateUserPasswordForm;
use App\Models\User;
use App\Requests\UpdateUserEmailRequest;
use App\Requests\UpdateUserPasswordRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserAdminController extends AbstractAdminController
{
    public function index(): Response
    {
        $request = Request::createFromGlobals();
        $page = (integer)($request->query->get('page') ?? 0);
        $search = $request->query->get('search');

        return $this->render('admin/users/index', [
            'usersPaginate' => $this->getRepository(User::class)->getByOrPaginate($page, 10, 'name', $search, [
                'createdAt' => 'DESC'
            ])
        ]);
    }

    public function show(int $userId): Response
    {
        $user = $this->getRepository(User::class)->find($userId);
        if (!$user) {
            return $this->redirectTo('/404');
        }

        return $this->render('admin/users/show', [
            'user' => $user,
            'comments' => $this->getRepository(User::class)->getComments($user),
            'posts' => $this->getRepository(User::class)->getPosts($user)
        ]);
    }

    public function updateEmail(int $userId): Response
    {
        $user = $this->getRepository(User::class)->find($userId);
        if ($user) {
            return $this->render('admin/users/update_email', [
                'form' => (new UpdateUserEmailForm($this->getSession()))->form($user)
            ]);
        }

        return $this->redirectTo('/404');
    }

    public function storeEmail(int $userId): Response
    {
        $repo = $this->getRepository(User::class);
        $updateUserEmailRequest = (new UpdateUserEmailRequest($this->getSession(), $repo))->rules();
        $user = $repo->find($userId);
        if ($user) {
            if ($updateUserEmailRequest->validate()) {
                $user->setEmail($updateUserEmailRequest->request->get('email'));
                $this->manager->persist($user);

                $this->getSession()->getFlashBag()
                    ->add('success', 'L\'adresse mail à bien modifié avec succès.');

                return $this->redirectTo('/admin/users');
            }
            return $this->redirectTo('/admin/users/' . $user->getId() . '/update-email');
        }

        return $this->redirectTo('/404');
    }

    public function updatePassword(int $userId): Response
    {
        $user = $this->getRepository(User::class)->find($userId);
        if ($user) {
            return $this->render('admin/users/update_password', [
                'form' => (new UpdateUserPasswordForm($this->getSession()))->form($user)
            ]);
        }

        return $this->redirectTo('/404');
    }

    public function storePassword(int $userId): Response
    {
        $updateUsePasswordRequest = (new UpdateUserPasswordRequest($this->getSession()))->rules();
        $user = $this->getRepository(User::class)->find($userId);
        if ($user) {
            if ($updateUsePasswordRequest->validate()) {
                $user->setPassword(password_hash($updateUsePasswordRequest->request->get('password'), PASSWORD_BCRYPT));
                $this->manager->persist($user);

                $this->getSession()->getFlashBag()
                    ->add('success', 'Le mot de passe à bien modifié avec succès.');

                return $this->redirectTo('/admin/users');
            }
            return $this->redirectTo('/admin/users/' . $user->getId() . '/update-password');
        }

        return $this->redirectTo('/404');
    }

    public function toggleAdmin(int $userId): Response
    {
        $user = $this->getRepository(User::class)->find($userId);
        if ($user) {
            $user->setIsAdmin(!$user->getIsAdmin());
            $this->manager->persist($user);

            $this->getSession()->getFlashBag()
                ->add('success', 'Le statut admin de l\'utilisateur à bien modifié avec succès.');

            return $this->redirectTo('/admin/users');
        }

        return $this->redirectTo('/404');
    }

    public function destroy(int $userId): Response
    {
        $user = $this->getRepository(User::class)->find($userId);
        if ($user) {
            $this->manager->remove($user);

            $this->getSession()->getFlashBag()
                ->add('success', 'L\'utilisateur à bien supprimé avec succès.');

            return $this->redirectTo('/admin/users');
        }

        return $this->redirectTo('/404');
    }
}