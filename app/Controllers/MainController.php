<?php

namespace App\Controllers;

use App\Models\Post;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('index', [
            'posts' => $this->getRepository(Post::class)->postsLatest()
        ]);
    }

    public function getCv(): Response
    {
        $file = '../public/pdf/cv.pdf';
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
        return $this->redirectTo('/');
    }
}