<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Response;

class ErrorController extends AbstractController
{
    public function error404(): Response
    {
        return $this->render('errors/error404');
    }
}