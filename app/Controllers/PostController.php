<?php

namespace App\Controllers;

use App\Forms\CommentForm;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Requests\StoreCommentRequest;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends AbstractController
{
    public function index(): Response
    {
        $request = Request::createFromGlobals();
        $page = (integer)($request->query->get('page') ?? 0);

        return $this->render('posts/index', [
            'postsPaginate' => $this->getRepository(Post::class)->paginate($page, 5)
        ]);
    }

    public function show(int $postId): Response
    {
        $post = $this->getRepository(Post::class)->find($postId);
        if (!$post) {
            return $this->redirectTo('/404');
        }

        return $this->render('posts/show', [
            'post' => $post,
            'comments' => $this->getRepository(Comment::class)->getCommentsOfPost($postId),
            'user' => $this->getRepository(User::class)->find($post->getUserId()),
            'form' => (new CommentForm($this->getSession()))->form($postId)
        ]);
    }

    public function storeComment(int $postId): Response
    {
        $storeCommentRequest = (new StoreCommentRequest($this->getSession()))->rules();
        if ($storeCommentRequest->validate()) {
            $comment = new Comment();
            $comment->setIsPublished(0);
            $comment->setContent($storeCommentRequest->request->get('content'));
            $comment->setPostId($postId);
            $comment->setUserId($this->getSession()->get('auth')->getId());
            $comment->setCreatedAt(new DateTime());

            $this->manager->persist($comment);

            $this->getSession()->getFlashBag()
                ->add('success', 'Le commentaire à bien été créé, il sera visible par 
                tous les utilisateurs après validation.');
        }

        return $this->redirectTo('/posts/' . $postId);
    }
}