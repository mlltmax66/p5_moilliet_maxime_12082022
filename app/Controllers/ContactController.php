<?php

namespace App\Controllers;

use App\Forms\ContactForm;
use App\Requests\ContactRequest;
use App\Services\MailService;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends AbstractController
{
    public function create(): Response
    {
        return $this->render('contact', [
            'form' => (new ContactForm($this->getSession()))->form()
        ]);
    }

    public function store(): Response
    {
        $contactRequest = (new ContactRequest($this->getSession()))->rules();
        if ($contactRequest->validate()) {

            $mailService = new MailService();
            $mailService->setSender($contactRequest->request->get('email'));
            $mailService->setReceiver('maxime.moilliet@email.com');
            $mailService->send(
                $contactRequest->request->get('subject'),
                $contactRequest->request->get('message')
            );

            $this->getSession()->getFlashBag()
                ->add('success', 'Votre message à bien été envoyé avec succès.');

            return $this->redirectTo('/contact');
        }

        return $this->redirectTo('/contact');
    }
}