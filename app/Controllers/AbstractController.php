<?php

namespace App\Controllers;

use App\Database\Database;
use App\Database\Manager;
use App\Database\ORMException;
use App\Services\AuthSession;
use App\Services\TwigRenderer;
use ReflectionException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;

abstract class AbstractController
{
    private TwigRenderer $renderer;

    private Database $database;

    private Session\Session $session;

    private AuthSession $authSession;

    protected Manager $manager;

    public function __construct()
    {
        $this->session = new Session\Session();
        $this->session->start();
        $this->renderer = new TwigRenderer($this->session);
        $this->database = Database::getInstance();
        $this->authSession = new AuthSession($this->session);
        $this->manager = new Manager($this->database);
    }

    public function render(string $view, array $params = [], string $status = Response::HTTP_OK): string|Response
    {
        return $this->renderer->render($view, $params, $status);
    }

    public function redirectTo(string $path): Response
    {
        return (new RedirectResponse($path))->send();
    }

    public function userIsLoggedOrRedirect()
    {
        if ($this->authSession->userIsLogged()) {
            return $this->redirectTo('/');
        }
    }

    public function userIsLoggedAndAdminOrRedirect()
    {
        if (!$this->authSession->userIsLoggedAndAdmin()) {
            return $this->redirectTo('/');
        }
    }

    public function getSession(): Session\Session
    {
        return $this->session;
    }

    public function getRepository(string $model)
    {
        return $this->database->getRepository($model);
    }
}
