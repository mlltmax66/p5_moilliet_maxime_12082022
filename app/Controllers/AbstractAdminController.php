<?php

namespace App\Controllers;

class AbstractAdminController extends AbstractController
{
    public function __construct()
    {
        parent::__construct();
        $this->userIsLoggedAndAdminOrRedirect();
    }
}