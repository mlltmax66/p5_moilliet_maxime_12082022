<?php

namespace App\Controllers\Auth;

use App\Controllers\AbstractController;
use App\Forms\Auth\LoginForm;
use App\Forms\Auth\RegisterForm;
use App\Models\User;
use App\Requests\LoginUserRequest;
use App\Requests\StoreUserRequest;
use DateTime;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractController
{
    public function __construct()
    {
        parent::__construct();
        $this->userIsLoggedOrRedirect();
    }

    public function loginView(): Response
    {
        return $this->render('auth/login', [
            'form' => (new LoginForm($this->getSession()))->form()
        ]);
    }

    public function login(): Response
    {
        $repo = $this->getRepository(User::class);
        $loginUserRequest = (new LoginUserRequest($this->getSession(), $repo))->rules();
        if ($loginUserRequest->validate()) {
            $this->getSession()->set('auth', $loginUserRequest->getUser());

            $this->getSession()->getFlashBag()
                ->add('success', 'Ravis de vous revoir ' . $loginUserRequest->getUser()->getName() . '.');

            return $this->redirectTo('/');
        }

        return $this->redirectTo('/login');
    }

    public function registerView(): Response
    {
        return $this->render('auth/register', [
            'form' => (new RegisterForm($this->getSession()))->form()
        ]);
    }

    public function register(): Response
    {
        $repo = $this->getRepository(User::class);
        $storeUserRequest = (new StoreUserRequest($this->getSession(), $repo))->rules();
        if ($storeUserRequest->validate()) {
            $user = new User();
            $user->setName($storeUserRequest->request->get('name'));
            $user->setEmail($storeUserRequest->request->get('email'));
            $user->setPassword(password_hash($storeUserRequest->request->get('password'), PASSWORD_BCRYPT));
            $user->setIsAdmin(0);
            $user->setCreatedAt(new DateTime());
            $user->setUpdatedAt(new DateTime());

            $this->manager->persist($user);

            $this->getSession()->getFlashBag()
                ->add('success', 'Votre compte à bien été créé ' . $user->getName() . '.');

            return $this->redirectTo('/login');
        }
        return $this->redirectTo('/register');
    }

    public function logout(): Response
    {
        $this->getSession()->clear();
        return $this->redirectTo('/');
    }
}
