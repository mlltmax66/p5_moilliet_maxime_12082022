<?php

namespace App\Requests;

use App\Database\Database;
use App\Models\User;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class UpdateUserEmailRequest extends AbstractRequest
{
    public function __construct(Session $session, private UserRepository $repo)
    {
        parent::__construct($session);
    }

    public function rules(): static
    {
        $this->validator->rule('required', 'email')
            ->message("Le champ adresse mail est obligatoire.");

        $this->validator->rule('email', 'email')
            ->message("Le champ adresse mail doit contenir une adresse mail valide.");

        $this->validator->rule(function () {
            $userEmailExist = $this->repo->findOneBy(['email' => $this->request->get('email')]);
            if ($userEmailExist) {
                return false;
            }
            return true;
        }, "email")->message("L'adresse mail renseigné existe déjà.");

        return $this;
    }
}