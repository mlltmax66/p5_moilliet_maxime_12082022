<?php

namespace App\Requests;

class StoreCommentRequest extends AbstractRequest
{
    public function rules(): static
    {
        $this->validator->rule('required', 'content')
            ->message("Le champ message est obligatoire.");

        $this->validator->rule('lengthMin', 'content', 8)
            ->message("Le champ message doit  contenir au moins 6 caractères.");

        return $this;
    }
}