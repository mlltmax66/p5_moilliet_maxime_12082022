<?php

namespace App\Requests;

use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class StoreUserRequest extends AbstractRequest
{
    public function __construct(Session $session, private UserRepository $repo)
    {
        parent::__construct($session);
    }

    public function rules(): static
    {
        $this->validator->rule('required', 'name')
            ->message("Le champ prénom et nom est obligatoire.");

        $this->validator->rule('required', 'email')
            ->message("Le champ adresse mail est obligatoire.");

        $this->validator->rule('email', 'email')
            ->message("Le champ adresse mail doit contenir une adresse mail valide.");

        $this->validator->rule('required', 'password')
            ->message("Le champ mot de passe est obligatoire.");

        $this->validator->rule('lengthMin', 'password', 8)
            ->message("Le champ mot de passe doit  contenir au moins 8 caractères.");

        $this->validator->rule('required', 'confirm_password')
            ->message("Le champ confirmer le mot de passe est obligatoire.");

        $this->validator->rule(function ($field, $value, $params, $fields) {
            if ($fields['confirm_password'] === $value) {
                return true;
            }
            return false;
        }, "password")->message("Le champ mot de passe et confimer le mot de passe doivent être indentiques.");

        $this->validator->rule(function () {
            $userEmailExist = $this->repo->findOneBy(['email' => $this->request->get('email')]);
            if ($userEmailExist) {
                return false;
            }
            return true;
        }, "email")->message("L'adresse mail renseigné existe déjà.");

        return $this;
    }
}
