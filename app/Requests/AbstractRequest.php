<?php

namespace App\Requests;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Valitron\Validator;

abstract class AbstractRequest
{
    protected Validator $validator;

    public Request $request;

    public function __construct(public Session $session)
    {
        $this->request = Request::createFromGlobals();
        $this->validator = new Validator($this->request->request->all());
    }

    public function validate(): bool
    {
        $this->validateCSRFToken();
        $this->validator->setPrependLabels(false);
        $valid = $this->validator->validate();
        $this->errors();
        return $valid;
    }

    private function errors(): void
    {
        $sessionName = 'errors_' . $this->request->get('unique_name');
        $this->session->getFlashBag()->add($sessionName, $this->validator->errors());
    }

    private function validateCSRFToken(): void
    {
        $this->validator->rule('required', 'csrf_token')->message('La présence du token csrf est obligatoire.');
        $this->validator->rule(function () {
            $sessionToken = $this->session->get('csrf_token_' . $this->request->get('unique_name'));
            if ($this->request->get('csrf_token') === $sessionToken) {
                return true;
            }
            return false;
        }, 'csrf_token')->message('Token csrf invalide.');
    }
}