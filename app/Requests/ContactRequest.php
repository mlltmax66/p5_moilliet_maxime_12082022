<?php

namespace App\Requests;

class ContactRequest extends AbstractRequest
{
    public function rules(): static
    {
        $this->validator->rule('required', 'email')
            ->message("Le champ adresse mail est obligatoire.");

        $this->validator->rule('email', 'email')
            ->message("Le champ adresse mail doit contenir une adresse mail valide.");

        $this->validator->rule('required', 'subject')
            ->message("Le champ sujet est obligatoire.");

        $this->validator->rule('required', 'message')
            ->message("Le champ message est obligatoire.");

        $this->validator->rule('lengthMin', 'message', 20)
            ->message("Le champ message doit  contenir au moins 20 caractères.");

        return $this;
    }
}