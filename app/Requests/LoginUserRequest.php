<?php

namespace App\Requests;

use App\Models\User;
use App\Repositories\UserRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginUserRequest extends AbstractRequest
{
    public User $user;

    public Session $session;

    public function __construct(Session $session, private UserRepository $repo)
    {
        parent::__construct($session);
    }

    public function rules(): static
    {
        $this->validator->rule('required', 'email')
            ->message("Le champ adresse mail est obligatoire.");

        $this->validator->rule('email', 'email')
            ->message("Le champ adresse mail doit contenir une adresse mail valide.");

        $this->validator->rule('required', 'password')
            ->message("Le champ mot de passe est obligatoire.");

        $this->validator->rule('lengthMin', 'password', 8)
            ->message("Le champ mot de passe doit  contenir au moins 8 caractères.");

        $this->validator->rule(function () {
            $userFirst = $this->repo->findOneBy(['email' => $this->request->get('email')]);
            if ($userFirst && password_verify($this->request->get('password'), $userFirst->getPassword())) {
                $this->user = $userFirst;
                return true;
            }
            return false;
        }, "email")->message("Les informations renseignées sont incorrectes.");

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}