<?php

namespace App\Requests;

class UpdateUserPasswordRequest extends AbstractRequest
{
    public function rules(): static
    {
        $this->validator->rule('required', 'password')
            ->message("Le champ mot de passe est obligatoire.");

        $this->validator->rule('lengthMin', 'password', 8)
            ->message("Le champ mot de passe doit  contenir au moins 8 caractères.");

        $this->validator->rule('required', 'confirm_password')
            ->message("Le champ confirmer le mot de passe est obligatoire.");

        $this->validator->rule(function ($field, $value, $params, $fields) {
            if ($fields['confirm_password'] === $value) {
                return true;
            }
            return false;
        }, "password")->message("Le champ mot de passe et confimer le mot de passe doivent être indentiques.");
        return $this;
    }
}