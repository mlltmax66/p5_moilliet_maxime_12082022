<?php

namespace App\Requests;

class StorePostRequest extends AbstractRequest
{
    public function rules(): static
    {
        $this->validator->rule('required', 'title')
            ->message("Le champ titre est obligatoire.");

        $this->validator->rule('required', 'teaser')
            ->message("Le champ synopsis est obligatoire.");

        $this->validator->rule('required', 'content')
            ->message("Le contenu est obligatoire.");
        return $this;
    }
}