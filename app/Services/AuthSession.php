<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Session;

class AuthSession
{
    private Session\Session $session;

    public function __construct(Session\Session $session)
    {
        $this->session = $session;
    }

    public function userIsLogged(): bool
    {
        $auth = $this->session->get('auth');
        if ($auth) {
            return true;
        }
        return false;
    }

    public function userIsLoggedAndAdmin(): bool
    {
        $auth = $this->session->get('auth');
        if ($this->userIsLogged()) {
            if ($auth->getIsAdmin()) {
                return true;
            }
            return false;
        }
        return false;
    }
}
