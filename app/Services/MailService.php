<?php

namespace App\Services;

use PHPMailer\PHPMailer\PHPMailer;

class MailService
{
    private PHPMailer $mailer;

    public function __construct()
    {
        $this->mailer = new PHPMailer(true);
        $this->mailer->isSMTP();
        $this->mailer->Host = getenv('MAIL_HOST');
        $this->mailer->SMTPAuth = true;
        $this->mailer->Port = getenv('MAIL_PORT');
        $this->mailer->Username = getenv('MAIL_USERNAME');
        $this->mailer->Password = getenv('MAIL_PASSWORD');
        $this->mailer->Charset = 'utf-8';
    }

    public function setReceiver(string $address): static
    {
        $this->mailer->addAddress($address);
        return $this;
    }

    public function setSender(string $address): static
    {
        $this->mailer->setFrom($address);
        return $this;
    }

    public function send(string $subject, string $message): void
    {
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $message;
        $this->mailer->send();
    }
}