<?php

namespace App\Database;

use App\Models\AbstractModel;

class Manager
{
    public function __construct(private Database $database)
    {
    }

    public function persist(AbstractModel $model): AbstractModel
    {
        if ($model->getPrimaryKey()) {
            return $this->update($model);
        }
        return $this->insert($model);
    }

    private function update(AbstractModel $model): AbstractModel
    {
        $set = [];
        $parameters = [];
        $metadata = $model::metadata();
        foreach (array_keys($metadata["columns"]) as $column) {
            $sqlValue = $model->getSQLValueByColumn($column);
            if ($sqlValue !== $model->originalData[$column]) {
                $parameters[$column] = $sqlValue;
                $model->orignalData[$column] = $sqlValue;
                $set[] = sprintf("%s = :%s", $column, $column);
            }
        }
        if (count($set)) {
            $sqlQuery = sprintf(/** @lang text */ "UPDATE %s SET %s WHERE %s = :id", $metadata["table"],
                implode(", ", $set), $metadata["primaryKey"]);
            $statement = $this->database->pdo->prepare($sqlQuery);
            $statement->execute(array_merge(["id" => $model->getPrimaryKey()], $parameters));
        }

        return $model;
    }

    private function insert(AbstractModel $model): AbstractModel
    {
        $set = [];
        $parameters = [];
        $metadata = $model::metadata();
        foreach (array_keys($metadata["columns"]) as $column) {
            $sqlValue = $model->getSQLValueByColumn($column);
            $model->orignalData[$column] = $sqlValue;
            $parameters[$column] = $sqlValue;
            $set[] = sprintf("%s = :%s", $column, $column);
        }
        $sqlQuery = sprintf(/** @lang text */ "INSERT INTO %s SET %s", $metadata["table"], implode(",", $set));
        $statement = $this->database->pdo->prepare($sqlQuery);
        $statement->execute($parameters);
        $model->setPrimaryKey($this->database->pdo->lastInsertId());

        return $model;
    }

    public function remove(AbstractModel $model): void
    {
        $data = $model::metadata();
        $sqlQuery = sprintf(/** @lang text */ "DELETE FROM %s WHERE %s = :id", $data["table"],
            $data["primaryKey"]);
        $statement = $this->database->pdo->prepare($sqlQuery);
        $statement->execute(["id" => $model->getPrimaryKey()]);
    }
}
