<?php

namespace App\Database;

class Database
{
    public \PDO $pdo;

    private array $repositories = [];

    private static ?Database $databaseInstance = null;

    public static function getInstance(): Database
    {
        if (self::$databaseInstance === null) {
            self::$databaseInstance = new Database();
        }
        return self::$databaseInstance;
    }

    public function __construct()
    {
        $this->pdo = new \PDO(getenv('DB_ADAPTER') .
            ":dbname=" . getenv('DB_NAME') . ";host=" . getenv('DB_HOST') .
            ';charset=utf8mb4', getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
    }

    public function getRepository(string $model): mixed
    {
        $repoClass = $model::getRepository();
        $this->repositories[$model] = $this->repositories[$model] ?? new $repoClass($this->pdo, $model);
        return $this->repositories[$model];
    }
}
