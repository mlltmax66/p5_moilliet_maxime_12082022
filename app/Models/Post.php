<?php

namespace App\Models;

use App\Repositories\PostRepository;
use DateTime;
use JetBrains\PhpStorm\ArrayShape;

class Post extends AbstractModel
{
    private ?int $id = null;

    private string $title;

    private string $teaser;

    private string $content;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    private int $userId;

    #[ArrayShape(["table" => "string", "primaryKey" => "string", "columns" => "\string[][]"])]
    public static function metadata(): array
    {
        return [
            "table" => "post",
            "primaryKey" => "id",
            "columns" => [
                "id" => [
                    "type" => "integer",
                    "property" => "id"
                ],
                "title" => [
                    "type" => "string",
                    "property" => "title"
                ],
                "teaser" => [
                    "type" => "string",
                    "property" => "teaser"
                ],
                "content" => [
                    "type" => "string",
                    "property" => "content"
                ],
                "created_at" => [
                    "type" => "datetime",
                    "property" => "createdAt"
                ],
                "updated_at" => [
                    "type" => "datetime",
                    "property" => "updatedAt"
                ],
                "user_id" => [
                    "type" => "integer",
                    "property" => "userId"
                ]
            ]
        ];
    }

    public static function getRepository(): string
    {
        return PostRepository::class;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTeaser(): string
    {
        return $this->teaser;
    }

    public function setTeaser(string $teaser): void
    {
        $this->teaser = $teaser;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }
}