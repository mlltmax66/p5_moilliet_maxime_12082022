<?php

namespace App\Models;

use App\Repositories\CommentRepository;
use DateTime;
use JetBrains\PhpStorm\ArrayShape;

class Comment extends AbstractModel
{
    private ?int $id = null;

    private string $content;

    private int $isPublished;

    private int $userId;

    private int $postId;

    private DateTime $createdAt;

    #[ArrayShape(["table" => "string", "primaryKey" => "string", "columns" => "\string[][]"])]
    public static function metadata(): array
    {
        return [
            "table" => "comment",
            "primaryKey" => "id",
            "columns" => [
                "id" => [
                    "type" => "integer",
                    "property" => "id"
                ],
                "content" => [
                    "type" => "string",
                    "property" => "content"
                ],
                "is_published" => [
                    "type" => "integer",
                    "property" => "isPublished"
                ],
                "user_id" => [
                    "type" => "integer",
                    "property" => "userId"
                ],
                "post_id" => [
                    "type" => "integer",
                    "property" => "postId"
                ],
                "created_at" => [
                    "type" => "datetime",
                    "property" => "createdAt"
                ]
            ]
        ];
    }

    public static function getRepository(): string
    {
        return CommentRepository::class;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getIsPublished(): int
    {
        return $this->isPublished;
    }

    public function setIsPublished(int $isPublished): void
    {
        $this->isPublished = $isPublished;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getPostId(): int
    {
        return $this->postId;
    }

    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}