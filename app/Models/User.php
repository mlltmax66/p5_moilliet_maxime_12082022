<?php

namespace App\Models;

use App\Repositories\UserRepository;
use DateTime;
use JetBrains\PhpStorm\ArrayShape;

class User extends AbstractModel
{
    private ?int $id = null;

    private string $name;

    private string $email;

    private string $password;

    private int $isAdmin;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    #[ArrayShape(["table" => "string", "primaryKey" => "string", "columns" => "\string[][]"])]
    public static function metadata(): array
    {
        return [
            "table" => "user",
            "primaryKey" => "id",
            "columns" => [
                "id" => [
                    "type" => "integer",
                    "property" => "id"
                ],
                "name" => [
                    "type" => "string",
                    "property" => "name"
                ],
                "email" => [
                    "type" => "string",
                    "property" => "email"
                ],
                "is_admin" => [
                    "type" => "integer",
                    "property" => "isAdmin"
                ],
                "password" => [
                    "type" => "string",
                    "property" => "password"
                ],
                "created_at" => [
                    "type" => "datetime",
                    "property" => "createdAt"
                ],
                "updated_at" => [
                    "type" => "datetime",
                    "property" => "updatedAt"
                ]
            ]
        ];
    }

    public static function getRepository(): string
    {
        return UserRepository::class;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getIsAdmin(): int
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(int $isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}