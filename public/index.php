<?php

use App\Services\Router;
use Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\RedirectResponse;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$dotenv = Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();

if (php_sapi_name() !== "cli") {
    $router = new Router();

    // Front
    $router->map('GET', '/', 'MainController#index');
    // CV
    $router->map('GET', '/cv', 'MainController#getCv');
    // Contact
    $router->map('GET', '/contact', 'ContactController#create');
    $router->map('POST', '/contact', 'ContactController#store');
    // Post and Add comment
    $router->map('GET', '/posts', 'PostController#index');
    $router->map('POST', '/posts/[i:id]/comment', 'PostController#storeComment');
    $router->map('GET', '/posts/[i:id]', 'PostController#show');

    // Auth
    $router->map('GET', '/login', 'Auth\\AuthController#loginView');
    $router->map('POST', '/login', 'Auth\\AuthController#login');
    $router->map('GET', '/register', 'Auth\\AuthController#registerView');
    $router->map('POST', '/register', 'Auth\\AuthController#register');
    $router->map('POST', '/logout', 'Auth\\AuthController#logout');

    // Admin
    $router->map('GET', '/admin', 'Admin\\DashboardAdminController#index');
    // Admin users
    $router->map('GET', '/admin/users', 'Admin\\UserAdminController#index');
    $router->map('POST', '/admin/users/[i:id]/destroy', 'Admin\\UserAdminController#destroy');
    $router->map('POST', '/admin/users/[i:id]/toggle-admin', 'Admin\\UserAdminController#toggleAdmin');
    $router->map('GET', '/admin/users/[i:id]/update-email', 'Admin\\UserAdminController#updateEmail');
    $router->map('POST', '/admin/users/[i:id]/store-email', 'Admin\\UserAdminController#storeEmail');
    $router->map('GET', '/admin/users/[i:id]/update-password', 'Admin\\UserAdminController#updatePassword');
    $router->map('POST', '/admin/users/[i:id]/store-password', 'Admin\\UserAdminController#storePassword');
    $router->map('GET', '/admin/users/[i:id]', 'Admin\\UserAdminController#show');
    // Admin posts
    $router->map('GET', '/admin/posts', 'Admin\\PostAdminController#index');
    $router->map('GET', '/admin/posts/create', 'Admin\\PostAdminController#create');
    $router->map('POST', '/admin/posts/create', 'Admin\\PostAdminController#store');
    $router->map('GET', '/admin/posts/[i:id]/edit', 'Admin\\PostAdminController#edit');
    $router->map('POST', '/admin/posts/[i:id]/update', 'Admin\\PostAdminController#update');
    $router->map('POST', '/admin/posts/[i:id]/destroy', 'Admin\\PostAdminController#destroy');
    // Admin comments
    $router->map('GET', '/admin/comments', 'Admin\\CommentAdminController#index');
    $router->map('POST', '/admin/comments/[i:id]/toggle-published', 'Admin\\CommentAdminController#togglePublished');
    $router->map('POST', '/admin/comments/[i:id]/destroy', 'Admin\\CommentAdminController#destroy');

    // Errors
    $router->map('GET', '/404', 'ErrorController#error404');

    $match = $router->match();
    if (!$match) {
        return (new RedirectResponse('/404'))->send();
    }
    $router->routerRequest($match['target'], $match['params']);
}


