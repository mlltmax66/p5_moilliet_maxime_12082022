const menu = document.querySelector("#menu")
const menuButton = document.querySelector("#menu-button")

menuButton && menuButton.addEventListener("click", () => {
    menu.style.display = menu.style.display === "block" ? "none" : "block"
})
