const alert = document.querySelector("#alert-success");
const alertCloseButton = alert ? alert.querySelector("button") : null;

alertCloseButton && window.setTimeout(() => {
    alert.style.display = "none";
}, 6000)

alertCloseButton && alertCloseButton.addEventListener("click", () => {
    alert.style.display = "none";
})